import { useContext, useState } from 'react';
import { ThemeContext } from 'styled-components';
import { Theme } from '../../../view/styles/types';

export function useDarkmode() {
  const theme = useContext(ThemeContext);
  const toggleDarkMode = () => theme.setTheme({ ...theme, isDarkMode: !theme.isDarkMode });
  return [theme.isDarkMode, toggleDarkMode];
}

export function useTheme(defaultTheme: Theme) {
  const [theme, setTheme] = useState(defaultTheme);
  return { ...theme, setTheme };
}

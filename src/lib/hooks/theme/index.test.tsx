import { render } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import { useDarkmode, useTheme } from '.';
import React from 'react';
import { defaultTheme } from '../../../view/styles/theme';
import { Theme } from '../../../view/styles/types';
import { act } from 'react-dom/test-utils';

function setup(inputTheme: Theme) {
  const ret = { isDarkMode: null, toggleDarkMode: null };
  function ThemeContainer(props: { children: React.ReactNode }) {
    const theme = useTheme(inputTheme);
    return <ThemeProvider theme={theme}>{props.children}</ThemeProvider>;
  }
  function MockCopmonent() {
    const [isDarkMode, toggleDarkMode] = useDarkmode();
    ret.isDarkMode = isDarkMode;
    ret.toggleDarkMode = toggleDarkMode;
    return null;
  }
  render(
    <ThemeContainer>
      <MockCopmonent />
    </ThemeContainer>,
  );
  return ret;
}

test('useDarkMode should return boolean and modifier', () => {
  const inputTheme = defaultTheme;
  const useDarkModeValue = setup(defaultTheme);
  expect(useDarkModeValue.isDarkMode).toEqual(inputTheme.isDarkMode);
  expect(typeof useDarkModeValue.toggleDarkMode).toEqual('function');
});

test('toggleDarkMode should change theme value', () => {
  const inputTheme = defaultTheme;
  const useDarkModeValue = setup(defaultTheme);
  act(() => (useDarkModeValue as any).toggleDarkMode());
  expect(useDarkModeValue.isDarkMode).toEqual(!inputTheme.isDarkMode);
});

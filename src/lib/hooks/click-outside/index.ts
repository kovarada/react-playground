import { useEffect } from 'react';

function useClickOutside(ref: React.MutableRefObject<HTMLElement | null>, callback: () => void) {
  const listener = (event: MouseEvent) => {
    if (ref.current && !ref.current.contains(event.target as Node)) {
      callback();
    }
  };
  useEffect(() => {
    document.addEventListener('click', listener);
    return () => {
      document.removeEventListener('click', listener);
    };
  });
}

export default useClickOutside;

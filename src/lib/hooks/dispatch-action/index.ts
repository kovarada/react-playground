import { Action } from 'redux';
import { useDispatch } from 'react-redux';
import { useCallback } from 'react';

export function useDispatchAction(action: Action) {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(action), [dispatch, action]);
}

import React from 'react';
import { render } from '@testing-library/react';
import useDocumentTitle from '.';

function MockCopmonent(props: { testTitle: string }) {
  useDocumentTitle(props.testTitle);
  return null;
}

it('sets document title', () => {
  document.title = 'default';
  const testTitle = 'test title';
  render(<MockCopmonent testTitle={testTitle} />);
  expect(document.title).toEqual(testTitle);
});

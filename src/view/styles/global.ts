import { createGlobalStyle } from 'styled-components';
import { Theme } from './types';

const GlobalStyle = createGlobalStyle`
    body {
        margin: 0;
        background-color: ${props => ((props.theme as Theme).isDarkMode ? '#404040' : 'white')};
        transition: background-color 200ms;
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
            'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
            sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
        monospace;
    }

    ::selection {
    background: #fbfba48a;
    }

    #root {
        overflow: hidden;
    }

    ul {
        margin:0;
        padding:0;
    }
`;

export default GlobalStyle;

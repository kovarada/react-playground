export type Theme = {
  isDarkMode: boolean;
  controlsBackgroundColor: string;
};

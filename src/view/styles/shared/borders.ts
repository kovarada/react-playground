import { css } from 'styled-components';

export const borderStyle = css`
  border: 1px solid ${props => props.theme.borderColor(props.theme.isDarkMode)};
`;

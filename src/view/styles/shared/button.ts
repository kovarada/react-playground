import styled from 'styled-components';
import { Theme } from '../types';

const BareButton = styled.button<{ theme: Theme; isActive?: boolean }>`
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  cursor: pointer;
  outline: inherit;
  text-align: center;
  text-decoration: none;
`;

export default BareButton;

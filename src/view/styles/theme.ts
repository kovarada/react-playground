export const defaultTheme = {
  isDarkMode: false,
  controlsBackgroundColor: '#d2d2d2',
  borderColor: (isDarkMode: boolean) => (isDarkMode ? '#97979773' : 'pink'),
};

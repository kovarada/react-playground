import React, { useCallback } from 'react';
import Polygon from '../../shapes/Polygon';
import { getSVGPoint } from './utils';
import { SVGMouseEvent } from '../../../../model/events';
import { useSelector, useDispatch } from 'react-redux';
import { getShapes, getCurrentShapeId, getCurrentShapeType } from '../../../../store/shapes/selectors';
import { getImage } from '../../../../store/image/selectors';
import styled from 'styled-components';
import ShapeType from '../../../../model/shapes/shape-type';
import { Polygon as PolygonModel, Label as LabelModel, Node as NodeModel } from '../../../../model/shapes';
import Label from '../../shapes/Label';
import { SVG_ELEMENT_DIMS, SVG_VIEWBOX_DIMS } from '../../../../utils/constants';
import { ShapeActionCreators } from '../../../../store/shapes/actions';
import Node from '../../shapes/Node';

const Panel = () => {
  const currentShapeId = useSelector(getCurrentShapeId);
  const shapes = useSelector(getShapes);
  const image = useSelector(getImage);
  const currentShapeType = useSelector(getCurrentShapeType);
  const dispatch = useDispatch();
  const draw = useCallback(
    (point: number[]) => dispatch(ShapeActionCreators.draw(currentShapeType, point)),
    [dispatch, currentShapeType],
  );

  const drawPoint = (event: SVGMouseEvent) => {
    if (event.target.nodeName === 'polygon') return;
    const newPoint = getSVGPoint(event);
    if (newPoint) draw(newPoint);
  };

  return (
    <Wrapper>
      <SVGDrawPanel
        width={SVG_ELEMENT_DIMS.width}
        height={SVG_ELEMENT_DIMS.height}
        viewBox={`0 0 ${SVG_VIEWBOX_DIMS.width} ${SVG_VIEWBOX_DIMS.height}`}
        onClick={drawPoint}
      >
        <Image {...image} />
        {shapes.map(
          shape =>
            shape.type === ShapeType.POLYGON && (
              <Polygon shape={shape as PolygonModel} key={shape._id} isActive={shape._id === currentShapeId} />
            ),
        )}
        {shapes.map(
          shape =>
            shape.type === ShapeType.NODE && (
              <Node shape={shape as NodeModel} key={shape._id} isActive={shape._id === currentShapeId} />
            ),
        )}
      </SVGDrawPanel>
      {shapes.map(
        shape =>
          shape.type === ShapeType.LABEL && (
            <Label shape={shape as LabelModel} key={shape._id} isActive={shape._id === currentShapeId} />
          ),
      )}
    </Wrapper>
  );
};

export default Panel;

const Wrapper = styled.section`
  overflow: hidden;
  position: relative;
`;

const SVGDrawPanel = styled.svg`
  background-color: white;
  z-index: 3;
`;

const Image = styled.image<{ isVisible: boolean }>`
  display: ${props => (props.isVisible ? 'auto' : 'none')};
`;

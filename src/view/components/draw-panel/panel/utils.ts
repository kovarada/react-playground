import { SVGMouseEvent } from '../../../../model/events';

export function getSVGPoint(event: SVGMouseEvent) {
  const svg = event.currentTarget as SVGSVGElement;
  if (!svg.createSVGPoint) return;
  const point = svg!.createSVGPoint() as SVGPoint;
  point.x = event.clientX;
  point.y = event.clientY;
  const cursor = point.matrixTransform(svg!.getScreenCTM()!.inverse());
  const newPoint = [cursor.x, cursor.y];
  return newPoint;
}

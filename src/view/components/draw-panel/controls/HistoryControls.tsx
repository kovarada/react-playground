import React from 'react';
import { ControlsWrapper, ControlsButtonStyle } from './style';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { canUndo, canRedo } from '../../../../store/history/selectors';
import { ActionCreators } from 'redux-undo';
import { useDispatchAction } from '../../../../lib/hooks/dispatch-action';

const HistoryControls = () => {
  const isUndoable = useSelector(canUndo);
  const isRedoable = useSelector(canRedo);
  const undo = useDispatchAction(ActionCreators.undo());
  const redo = useDispatchAction(ActionCreators.redo());

  return (
    <Wrapper>
      <Button title="Go back" className="fa fa-arrow-left" isActive={isUndoable} onClick={undo} />
      <Button title="Go forward" className="fa fa-arrow-right" isActive={isRedoable} onClick={redo} />
    </Wrapper>
  );
};

export default HistoryControls;

const Wrapper = styled(ControlsWrapper)`
  overflow: hidden;
  top: 0;
  left: 0;
`;

const Button = styled(ControlsButtonStyle)``;

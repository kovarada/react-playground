import styled, { css } from 'styled-components';
import BareButton from '../../../styles/shared/button';

export const controlsStyle = css`
  background-color: ${props => props.theme.controlsBackgroundColor};
  border-radius: 3px;
  border: none;
`;

export const ControlsWrapper = styled.div<{ isVisible?: boolean }>`
  ${controlsStyle}
  position: absolute;
  width: min-content;
  display: flex;
  transition: all 200ms;
`;

export const ControlsInputStyle = styled.input`
  ${controlsStyle}
  background-color: #80808033;
  color: grey;
  outline: none;
`;

export const ControlsButtonStyle = styled(BareButton)<{ isVisible?: boolean }>`
  display: ${props => (props.isVisible === false ? 'none' : 'auto')};
  pointer-events: ${props => (props.isActive !== false ? 'auto' : 'none')};
  border-radius: 3px;
  color: ${props => (props.isActive !== false ? 'grey' : '#8080802b')};
  padding: 5px;
  &:hover {
    background-color: #80808033;
    cursor: pointer;
  }
  &:active {
    background-color: #8080800d;
  }
`;

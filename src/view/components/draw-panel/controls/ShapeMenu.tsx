import React, { useCallback } from 'react';
import styled, { css, keyframes } from 'styled-components';
import { controlsStyle, ControlsButtonStyle } from './style';
import ShapeType from '../../../../model/shapes/shape-type';
import { useDispatch, useSelector } from 'react-redux';
import { getCurrentShapeType } from '../../../../store/shapes/selectors';
import { ShapeActionCreators } from '../../../../store/shapes/actions';

const ShapeMenu = () => {
  const dispatch = useDispatch();
  const setCurrentShapeType = useCallback(
    (shapeType: ShapeType) => dispatch(ShapeActionCreators.setCurrentShapeType(shapeType)),
    [dispatch],
  );
  const currentShapeType = useSelector(getCurrentShapeType);
  return (
    <Wrapper>
      <Button isVisible={currentShapeType === ShapeType.POLYGON} onClick={() => setCurrentShapeType(ShapeType.POLYGON)}>
        <Label>Polygon</Label>
        <Icon className="fa fa-square-o"></Icon>
      </Button>
      <Button isVisible={currentShapeType === ShapeType.LABEL} onClick={() => setCurrentShapeType(ShapeType.LABEL)}>
        <Label>Label</Label>
        <Icon className="fa fa-font"></Icon>
      </Button>
      <Button isVisible={currentShapeType === ShapeType.NODE} onClick={() => setCurrentShapeType(ShapeType.NODE)}>
        <Label>Node</Label>
        <Icon className="fa fa-dot-circle-o"></Icon>
      </Button>
    </Wrapper>
  );
};

export default ShapeMenu;

const Wrapper = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  transform: translateX(101%);
`;

const labelWidth = '5em';

const slideIn = keyframes`
  from {
    width:0;
  }
  to {
    width:${labelWidth};
  }
`;

const slideOut = keyframes`
  from {
    width:${labelWidth};
  }
  to {
    width:0;
  }
`;

const Button = styled(ControlsButtonStyle)`
  ${controlsStyle};
  background-color: ${props => (props.isVisible ? props.theme.controlsBackgroundColor : '#e6e6e6')};
  display: flex;
  border-bottom-left-radius: 0;
  border-top-left-radius: 0;
  padding: 0;
  justify-content: center;
  align-items: center;
  max-height: 3vh;
  overflow: hidden;
  width: min-content;
  pointer-events: ${props => (props.isVisible ? 'none' : 'all')};
  & > code {
    animation: ${props => (props.isVisible ? slideIn : slideOut)} 200ms ease-out forwards;
    border-right-width: ${props => (props.isVisible ? '1px' : '0px')};
  }
`;

const buttonContentStyle = css`
  padding: 5px;
`;

const Label = styled.code`
  ${buttonContentStyle}
  padding: 0;
  width: 0;
  border: 0 solid grey;
  border-right-width: 1px;
  overflow: hidden;
`;

const Icon = styled.span`
  ${buttonContentStyle}
  margin-top:2px;
`;

import React, { useCallback } from 'react';
import styled, { keyframes } from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { getCurrentShape } from '../../../../store/shapes/selectors';
import { ControlsWrapper, ControlsButtonStyle } from './style';
import ShapeAttribsInput from './ShapeAttribsInput';
import { ShapeActionCreators } from '../../../../store/shapes/actions';
import { isShapeUpToDate } from '../../../../utils/shapes';

const ShapeControls = () => {
  const dispatch = useDispatch();
  const undoPoint = useCallback(() => dispatch(ShapeActionCreators.undoShape()), [dispatch]);
  const redoPoint = useCallback(() => dispatch(ShapeActionCreators.redoShape()), [dispatch]);
  const saveShape = useCallback(() => dispatch(ShapeActionCreators.saveShape()), [dispatch]);
  const currentShape = useSelector(getCurrentShape);
  const deleteShape = useCallback(() => {
    if (currentShape) dispatch(ShapeActionCreators.deleteShape(currentShape));
  }, [dispatch, currentShape]);

  return (
    <Wrapper>
      <ButtonsWrapper isVisible={currentShape !== null}>
        <Button className="fa fa-arrow-left" onClick={undoPoint} title="Undo action for this element" />
        <Button
          className="fa fa-arrow-right"
          isActive={!isShapeUpToDate(currentShape)}
          onClick={redoPoint}
          title="Redo action for this element"
        />
        <Button className="fa fa-floppy-o" onClick={() => saveShape()} title="Save shape" />
        <ShapeAttribsInput shape={currentShape} />
        <Button className="fa fa-trash-o" onClick={deleteShape} title="Delete shape" />
      </ButtonsWrapper>
    </Wrapper>
  );
};

export default ShapeControls;

const Wrapper = styled.div`
  position: absolute;
  overflow: hidden;
  top: 0px;
  left: 50%;
  transform: translateX(-50%);
`;

const slideIn = keyframes`
  from {
    transform: translateY(-100%);
  }
  to {
    transform: translateY(0%);
  }
`;

const slideOut = keyframes`
  from {
    transform: translateY(0%);
  }
  to {
    transform: translateY(-100%);
  }
`;

const ButtonsWrapper = styled(ControlsWrapper)`
  position: relative;
  animation: ${props => (props.isVisible ? slideIn : slideOut)} 200ms ease-out forwards;
`;

const Button = styled(ControlsButtonStyle)``;

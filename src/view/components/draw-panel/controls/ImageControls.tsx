import React, { useCallback, useState } from 'react';
import styled, { css, keyframes } from 'styled-components';
import Image from '../../../../model/image';
import { useDispatch, useSelector } from 'react-redux';
import { ControlsWrapper, ControlsInputStyle, ControlsButtonStyle } from './style';
import { getImage } from '../../../../store/image/selectors';
import { ImageActionCreators } from '../../../../store/image/actions';

type FormState = 'expanded' | 'hidden' | 'none';

function ImageControls() {
  const dispatch = useDispatch();
  const setImage = useCallback((image: Image) => dispatch(ImageActionCreators.setImage(image)), [dispatch]);
  const toggleImageVisibility = useCallback(() => dispatch(ImageActionCreators.toggleImageVisibility()), [dispatch]);
  const image = useSelector(getImage);
  const [formState, setFormState] = useState('none' as FormState);
  const [href, setHref] = useState(image.href);
  const [width, setWidth] = useState(image.width);
  const [height, setHeight] = useState(image.height);

  const handleFormButton = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    setImage({ href: href, width: width, height: height });
  };

  const toggleFormState = () => {
    if (formState === 'expanded') {
      setFormState('hidden');
    } else {
      setFormState('expanded');
    }
  };

  const isFormVisible = formState === 'expanded';

  return (
    <Wrapper>
      <ImageForm visibility={formState}>
        <Label>src:</Label>
        <HrefInput value={href} placeholder="./panel-background.png" onChange={event => setHref(event.target.value)} />
        <Label>width:</Label>
        <SizeInput value={width} onChange={event => setWidth(event.target.value)} />
        <Label>height:</Label>
        <SizeInput value={height} onChange={event => setHeight(event.target.value)} />
        <Button className="fa fa-arrow-right" onClick={handleFormButton} />
      </ImageForm>
      <Button className={`fa fa-${isFormVisible ? 'angle-left' : 'picture-o'}`} onClick={toggleFormState} />
      <Button
        className={`fa fa-eye${!image.isVisible ? '-slash' : ''}`}
        isVisible={!isFormVisible}
        onClick={toggleImageVisibility}
      />
    </Wrapper>
  );
}

export default ImageControls;

const Wrapper = styled(ControlsWrapper)`
  bottom: 0;
`;

const inputStyle = css`
  margin: 2px 5px;
  height: 100%;
`;

const HrefInput = styled(ControlsInputStyle)`
  ${inputStyle}
  width: 150px;
`;

const SizeInput = styled(ControlsInputStyle)`
  ${inputStyle}
  width: 40px;
`;

const slideIn = keyframes`
  from {
    width: 0;
  }
  to {
    width: 430px;
  }
`;

const slideOut = keyframes`
  from {
    width: 430px;
  }
  to {
    width: 0;
  }
`;

const ImageForm = styled.form<{ visibility: FormState }>`
  overflow: hidden;
  width: 0;
  max-height: 25px;
  animation-name: ${props =>
    props.visibility === 'none' ? 'none' : props.visibility === 'expanded' ? slideIn : slideOut};
  animation-duration: 200ms;
  animation-timing-function: ease-out;
  animation-fill-mode: forwards;
`;

const Label = styled.label`
  color: grey;
  margin: 3px;
`;

const Button = styled(ControlsButtonStyle)``;

import React, { useCallback, useState } from 'react';
import Shape, { PolygonAttribs, LabelAttribs, NodeAttribs } from '../../../../model/shapes';
import ShapeType from '../../../../model/shapes/shape-type';
import styled from 'styled-components';
import { ControlsInputStyle, ControlsButtonStyle } from './style';
import { getShapeAttribs } from '../../../../utils/shapes';
import { useDispatch } from 'react-redux';
import { copyInputToClipboard } from '../../utils';
import { PolygonActionCreators } from '../../../../store/polygons/actions';
import { ShapeActionCreators } from '../../../../store/shapes/actions';
import { LabelActionCreators } from '../../../../store/labels/actions';
import { NodeActionCreators } from '../../../../store/nodes/actions';

type Props = {
  shape: Shape;
  isSrcVisible: boolean;
};

const ShapeAttribsInput = (props: Props) => {
  const { shape, isSrcVisible } = props;
  const dispatch = useDispatch();
  const setPoints = useCallback((points: string) => dispatch(PolygonActionCreators.setPoints(points)), [dispatch]);
  const setLabelPosition = useCallback(
    (position: number[]) => dispatch(LabelActionCreators.setLabelPosition(position)),
    [dispatch],
  );
  const setNodePosition = useCallback(
    (position: number[]) => dispatch(NodeActionCreators.setNodePosition(position)),
    [dispatch],
  );
  const saveShape = useCallback(() => dispatch(ShapeActionCreators.saveShape()), [dispatch]);

  const handleSpecialKeys = (event: React.KeyboardEvent<HTMLInputElement>) => {
    switch (event.key) {
      case 'Escape':
        saveShape();
        break;
      case 'Enter':
        saveShape();
        break;
      default:
        break;
    }
  };

  if (!isSrcVisible) return null;
  switch (shape.type) {
    case ShapeType.POLYGON:
      return (
        <>
          <Input
            value={(getShapeAttribs(shape) as PolygonAttribs) || ''}
            onChange={event => setPoints(event.target.value)}
            onKeyDown={handleSpecialKeys}
          />
          <CopyButton
            title="Copy coordinates to clipboard"
            className="fa fa-clipboard"
            onClick={() => copyInputToClipboard((getShapeAttribs(shape) as string) || '', 'Points')}
          />
        </>
      );
    case ShapeType.LABEL:
      const { x, y } = getShapeAttribs(shape) as LabelAttribs;
      const handleArrowKeys = (event: React.KeyboardEvent<HTMLInputElement>, target: 'x' | 'y') => {
        const point = [x, y];
        const targetIdx = target === 'x' ? 0 : 1;
        switch (event.key) {
          case 'ArrowUp':
            point[targetIdx] = point[targetIdx] + 1;
            break;
          case 'ArrowDown':
            point[targetIdx] = point[targetIdx] - 1;
            break;
          default:
            handleSpecialKeys(event);
            return;
        }
        setLabelPosition(point);
      };
      return (
        <>
          <InputLabel>x:</InputLabel>
          <LabelPositionInput
            value={x || ''}
            onChange={event => setLabelPosition([+event.target.value, y])}
            onKeyDown={event => handleArrowKeys(event, 'x')}
          />
          <InputLabel>y:</InputLabel>
          <LabelPositionInput
            value={y || ''}
            onChange={event => setLabelPosition([x, +event.target.value])}
            onKeyDown={event => handleArrowKeys(event, 'y')}
          />
        </>
      );
    case ShapeType.NODE:
      const nodeAttribs = getShapeAttribs(shape) as NodeAttribs;
      if (!nodeAttribs) return null;
      return (
        <>
          <InputLabel>x:</InputLabel>
          <LabelPositionInput
            value={nodeAttribs.x}
            type="number"
            onChange={event => setNodePosition([+event.target.value, nodeAttribs.y])}
          />
          <InputLabel>y:</InputLabel>
          <LabelPositionInput
            value={nodeAttribs.y}
            type="number"
            onChange={event => setNodePosition([nodeAttribs.x, +event.target.value])}
          />
        </>
      );
    default:
      return null;
  }
};

type WrapperProps = {
  shape: Shape | null;
};

const WrappedAttribsInput = ({ shape }: WrapperProps) => {
  const [isSrcVisible, setIsSrcVisible] = useState(false);
  if (!shape) return null;

  return (
    <>
      <ShapeAttribsInput shape={shape} isSrcVisible={isSrcVisible} />
      <Button
        title={!isSrcVisible ? 'Edit coordinates' : 'Close'}
        className={`fa fa-${isSrcVisible ? 'angle-left' : 'code'}`}
        onClick={() => setIsSrcVisible(!isSrcVisible)}
      />
    </>
  );
};

export default WrappedAttribsInput;

const Input = styled(ControlsInputStyle)`
  border-bottom-right-radius: 0;
  border-top-right-radius: 0;
`;

const LabelPositionInput = styled(Input)`
  border-radius: 3px;
  width: 3em;
  margin: 0 3px;
`;

const Button = styled(ControlsButtonStyle)``;

const CopyButton = styled(Button)`
  background-color: ${props => props.theme.controlsBackgroundColor};
  border-bottom-left-radius: 0;
  border-top-left-radius: 0;
`;

const InputLabel = styled.label`
  color: grey;
  margin: 0 3px;
`;

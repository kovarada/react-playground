import React from 'react';
import HistoryControls from './controls/HistoryControls';
import ShapeMenu from './controls/ShapeMenu';
import Panel from './panel';
import ShapeControls from './controls/ShapeControls';
import ImageControls from './controls/ImageControls';
import { borderStyle } from '../../styles/shared/borders';
import styled from 'styled-components';

const DrawPanel = () => {
  return (
    <Wrapper>
      <Panel />
      <HistoryControls />
      <ShapeControls />
      <ImageControls />
      <ShapeMenu />
    </Wrapper>
  );
};

export default DrawPanel;

const Wrapper = styled.section`
  ${borderStyle}
  position: relative;
  grid-area: 2 / 2;
  margin: 0 auto;
  width: min-content;
  height: min-content;
  border-radius: 5px;
  position: sticky;
  top: 100px;
  transform-origin: top;
  transform: scale(1.3);
`;

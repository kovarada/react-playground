import React, { useCallback, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import Shape, { Node, Polygon } from '../../../model/shapes';
import ShapeCreators from '../../../model/shapes/creators';
import ShapeType from '../../../model/shapes/shape-type';
import { data } from '../../../model/templates/data';
import { ActionType } from '../../../store/ActionType';
import { getShapes } from '../../../store/shapes/selectors';
import BareButton from '../../styles/shared/button';
import { copyInputToClipboard } from '../utils';
import { findClosestNode, getPolygonCenter, Position } from './utils';

type Props = {};

export const ExportPanel: React.FC<Props> = () => {
  const shapes = useSelector(getShapes);
  const dispatch = useDispatch();
  const [nodes, polygons] = useMemo(
    () => [ShapeType.NODE, ShapeType.POLYGON].map(shapeType => shapes.filter(({ type }) => type === shapeType)),
    [shapes],
  );
  const [floor, setFloor] = useState(0);

  const exportShapes = useCallback(
    (shapes: Shape[]) => {
      const exportString = shapes
        .map(node => node.toString(node, floor))
        .reduce((prev, next) => `${prev}\n${next}`, '');
      copyInputToClipboard(exportString, 'Shapes');
    },
    [floor],
  );

  const exportNodes = useCallback(() => {
    // previously created roomNodes
    const currentRoomNodes = nodes.filter(node => {
      const roomNode = polygons.find(polygon => polygon.id === node.id);
      return roomNode !== undefined;
    });
    // filter previously created roomNodes
    const exportNodes = nodes.filter(
      node => currentRoomNodes.find(roomNode => roomNode.id === node.id) === undefined,
    ) as Node[];

    // remove roomNodes from children
    exportNodes.forEach(node => {
      node.history[node.historyIdx].children = node.history[node.historyIdx].children.filter(
        child => currentRoomNodes.find(node => node.id === child.id) === undefined,
      );
    });

    // create tuples, room id - { closest node and polygon center }
    const roomMap = (polygons as Polygon[])
      .map(polygon => [polygon.history[polygon.historyIdx], polygon.id] as [string, string])
      .map(([points, id]) => [getPolygonCenter(points), id] as [Position, string])
      .map(([center, id]) => ({ id, nodeData: { closestNode: findClosestNode(center, exportNodes), center } }))
      .filter(({ nodeData }) => nodeData.closestNode !== null);

    // create room nodes with connected closest node
    const roomNodes = roomMap.map(({ id, nodeData }) => {
      const { center, closestNode } = nodeData;
      const newNode = ShapeCreators.node([center.x, center.y]);
      newNode.id = id;

      if (closestNode !== null) {
        newNode.history[newNode.historyIdx].children.push(closestNode);
        closestNode.history[closestNode.historyIdx].children.push(newNode);
      }
      return newNode;
    });

    exportNodes.push(...roomNodes);
    dispatch({ type: ActionType.SET_NODES, nodes: exportNodes });
    exportShapes(exportNodes);
  }, [polygons, nodes, exportShapes, dispatch]);

  const isAnyShapePresent = nodes.length > 0 || polygons.length > 0;

  const setTemplate = useCallback(
    (id: string) => {
      if (isAnyShapePresent) {
        const response = window.confirm('Are you sure? Setting template will delete all existing shapes!');
        if (!response) {
          return;
        }
      }
      dispatch({ type: ActionType.SET_TEMPLATE, id });
    },
    [dispatch, isAnyShapePresent],
  );

  return (
    <S.Wrapper>
      <S.TemplateSelect title={'Choose a building template with polygons'}>
        <summary>Pick template</summary>
        <S.TemplateSelectList>
          {Object.keys(data).map(templateId => (
            <button key={templateId} value={templateId} onClick={() => setTemplate(templateId)}>
              {templateId}
            </button>
          ))}
        </S.TemplateSelectList>
      </S.TemplateSelect>
      {isAnyShapePresent && <h4>Export shapes</h4>}
      {nodes.length > 0 && (
        <>
          <S.FloorInput
            type="number"
            title="Nodes will be generated with this floor in coordinates"
            placeholder="floor"
            onChange={event => setFloor(Number(event.target.value))}
          />
          <ExportNodesButton
            onClick={exportNodes}
            title="This will also create node for each polygon and connect it to the nearest node"
          >
            Export all nodes
          </ExportNodesButton>
        </>
      )}
      {polygons.length > 0 && (
        <ExportPolygonsButton onClick={() => exportShapes(polygons)}>Export all polygons</ExportPolygonsButton>
      )}
    </S.Wrapper>
  );
};

const ExportNodesButton = styled(BareButton)`
  left: auto;
  right: 0;
  background-color: #c5ffc5;
  padding: 20px;
  border-radius: 5px;
`;

const ExportPolygonsButton = styled(ExportNodesButton)`
  background-color: #ffd5c4;
`;

const S = {
  Wrapper: styled.div`
    position: absolute;
    left: auto;
    right: 0;
    margin: 100px 15px;
    background-color: #ffffffdb;
    padding: 20px;
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    box-shadow: 0 0 14px 2px #bdbcbce8;
    gap: 1rem;

    & > h4 {
      font-size: 1rem;
      font-weight: 200;
      margin: 0;
      margin-top: 1rem;
    }

    &:empty {
      display: none;
    }
  `,

  FloorInput: styled.input``,

  TemplateSelect: styled(BareButton).attrs({ as: 'details' })`
    border-radius: 5px;
    padding: 5px;
    border: 1px solid #afafaf;
    background-color: white;

    & > summary {
      padding: 3px;
      font-size: 0.9rem;
    }

    &[open] > summary {
      border-bottom: 1px solid #d1d1d1;
    }
  `,

  TemplateSelectList: styled.ul`
    margin-top: 5px;

    & > button {
      background-color: white;
      border: none;
      cursor: pointer;
      width: 100%;

      &:hover {
        background-color: #f3f3f3;
      }

      &:not(:last-child) {
        border-bottom: 1px dashed #d1d1d1;
      }
    }
  `,
};

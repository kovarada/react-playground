import { Node } from '../../../model/shapes';

export function getPolygonCenter(points: string) {
  function reduceSum(sum: number, point: number): number {
    return sum + point;
  }
  const pointStringArray = points.split(' ');
  const pointArray = pointStringArray
    .filter(point => point.length > 0)
    .map(point => point.split(',') as [string, string])
    .map(coords => [Number(coords[0]), Number(coords[1])]);
  const pointCount = pointArray.length;

  const xValues = pointArray.map(([x]) => x);
  const yValues = pointArray.map(([, y]) => y);

  const x = xValues.reduce(reduceSum, 0) / pointCount;
  const y = yValues.reduce(reduceSum, 0) / pointCount;

  return { x, y };
}

export type Position = { x: number; y: number };

export function findClosestNode(position: Position, nodes: Node[]) {
  if (nodes.length === 0) {
    return null;
  }

  function getDistance(a: typeof position, b: typeof position) {
    const [distX, distY] = [b.x - a.x, b.y - a.y];
    return Math.sqrt(distX ** 2 + distY ** 2);
  }

  return nodes.reduce((prevNode, currentNode) => {
    const [prevDistance, currentDistance] = [prevNode, currentNode].map(node => {
      return getDistance(position, node.history[node.historyIdx]);
    });
    if (currentDistance < prevDistance) {
      return currentNode;
    }
    return prevNode;
  }, nodes[0]);
}

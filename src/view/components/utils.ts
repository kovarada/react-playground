import store from '../../store';
import { ActionType } from '../../store/ActionType';

export const blurInputOnEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
  if (event.key === 'Enter') event.currentTarget.blur();
};

export function copyInputToClipboard(input: string, contentType: string) {
  window.navigator.clipboard.writeText(input);
  store.dispatch({ type: ActionType.SET_MESSAGE, message: `${contentType} copied to clipboard` });
}

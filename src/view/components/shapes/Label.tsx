import React, { useCallback } from 'react';
import styled, { css } from 'styled-components';
import { Label as LabelModel } from '../../../model/shapes';
import { useDispatch } from 'react-redux';
import { ShapeActionCreators } from '../../../store/shapes/actions';

type Props = { shape: LabelModel; isActive: boolean };

const Label = (props: Props) => {
  const { history, historyIdx } = props.shape;
  const { isActive, shape } = props;
  const labelAttribs = history[historyIdx];
  const dispatch = useDispatch();
  const setCurrentShapeId = useCallback((_id: string | null) => dispatch(ShapeActionCreators.setCurrentShapeId(_id)), [
    dispatch,
  ]);
  const setShapeId = useCallback((newId: string) => dispatch(ShapeActionCreators.setShapeId(shape, newId)), [
    dispatch,
    shape,
  ]);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setShapeId(event.target.value);
  };

  const handleSpecialKeys = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key !== 'Enter' && event.key !== 'Escape') return;
    event.currentTarget.blur();
    setCurrentShapeId(null);
  };

  return (
    <Wrapper
      style={{ left: labelAttribs.x, top: labelAttribs.y }}
      isActive={isActive}
      onClick={() => setCurrentShapeId(shape._id)}
    >
      <IDInput value={shape.id} onChange={handleInputChange} onKeyDown={handleSpecialKeys} />
    </Wrapper>
  );
};

export default Label;

const Wrapper = styled.div<{ isActive: boolean }>`
  position: absolute;
  width: min-content;
  z-index: 4;
  ${props =>
    props.isActive &&
    css`
      border-style: dotted;
      border-color: grey;
      border-width: 1px;
      border-radius: 3px;
    `}
`;

const IDInput = styled.input`
  background-color: transparent;
  border: none;
  width: 2em;
  outline: none;
`;

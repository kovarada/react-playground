import React, { useCallback, useState } from 'react';
import { Node as NodeModel, NodeAttribs } from '../../../model/shapes';
import { getShapeAttribs } from '../../../utils/shapes';
import { useDispatch, useSelector } from 'react-redux';
import { ShapeActionCreators } from '../../../store/shapes/actions';
import styled from 'styled-components';
import { getCurrentShape, getShapes } from '../../../store/shapes/selectors';
import { NodeActionCreators } from '../../../store/nodes/actions';
import ShapeType from '../../../model/shapes/shape-type';

type Props = { shape: NodeModel; isActive: boolean };

const Node = (props: Props) => {
  const { shape, isActive } = props;
  const { x, y, ...attribs } = getShapeAttribs(props.shape) as NodeAttribs;
  const currentShape = useSelector(getCurrentShape) as NodeModel;
  const shapes = useSelector(getShapes);
  const children = shapes.filter(shape => attribs.children.find(child => child._id === shape._id));
  const [isHover, setIsHover] = useState(false);
  const dispatch = useDispatch();
  const setCurrentShapeId = useCallback(
    () => dispatch(ShapeActionCreators.setCurrentShapeId(shape._id)),
    [dispatch, shape._id],
  );
  const connectNode = useCallback(() => dispatch(NodeActionCreators.connectNode(shape)), [dispatch, shape]);

  const handleClick = (event: React.MouseEvent<SVGCircleElement, MouseEvent>) => {
    event.stopPropagation();
    if (currentShape?.type === ShapeType.NODE) {
      connectNode();
    } else {
      setCurrentShapeId();
    }
  };

  const renderPath = (child: NodeModel) => {
    if (!child) return null;
    const childAttribs = getShapeAttribs(child) as NodeAttribs;
    return (
      <SVGPath
        isActive={isActive || (!currentShape && isHover)}
        points={`${x},${y} ${childAttribs.x},${childAttribs.y}`}
        key={`${child._id}-${props.shape._id}`}
      />
    );
  };

  return (
    <>
      {isHover && renderPath(currentShape)}
      {children.map(nodeChild => renderPath(nodeChild as NodeModel))}
      <SVGNode
        cx={x}
        cy={y}
        isActive={isActive}
        onClick={handleClick}
        onMouseOver={() => setIsHover(true)}
        onMouseLeave={() => setIsHover(false)}
      />
    </>
  );
};
export default Node;

const SVGNode = styled.circle<{ isActive: boolean }>`
  stroke: ${props => (!props.isActive ? '#ff7e94' : '#ff00006b')};
  r: 4;
  stroke-width: 1;
  fill: ${props => (!props.isActive ? '#ffc0cb75' : 'pink')};

  &:hover {
    cursor: pointer;
    stroke-width: 2;
  }
`;

const SVGPath = styled.polyline<{ isActive: boolean }>`
  stroke: #ffc0cb75;
  stroke-width: 4;
  stroke-opacity: ${props => (props.isActive ? 1 : 0.5)};
  stroke-dasharray: ${props => (props.isActive ? 0 : '5px')};
`;

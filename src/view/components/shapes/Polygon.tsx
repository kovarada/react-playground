import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import styled, { css } from 'styled-components';
import { Polygon as PolygonModel, PolygonAttribs } from '../../../model/shapes';
import { ShapeActionCreators } from '../../../store/shapes/actions';
import { getShapeAttribs } from '../../../utils/shapes';

type Props = { shape: PolygonModel; isActive: boolean };

const Polygon = (props: Props) => {
  const { shape, isActive } = props;
  const points = getShapeAttribs(props.shape) as PolygonAttribs;
  const dispatch = useDispatch();
  const setCurrentShapeId = useCallback(() => dispatch(ShapeActionCreators.setCurrentShapeId(shape._id)), [
    dispatch,
    shape._id,
  ]);
  return <SVGPolygon points={points} onClick={setCurrentShapeId} isActive={isActive} />;
};

export default Polygon;

const SVGPolygon = styled.polygon<{ isActive?: boolean }>`
  transition: all 200ms;
  stroke-width: 1.5;
  fill-opacity: 0;
  stroke: ${props => (props.isActive ? 'grey' : '#000')};

  ${props =>
    props.isActive
      ? css`
          pointer-events: none;
        `
      : css`
          &:hover {
            cursor: pointer;
            fill: grey;
            fill-opacity: 0.25;
          }
        `}
`;

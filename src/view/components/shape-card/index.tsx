import React, { useCallback, useEffect, useRef, useState } from 'react';
import styled, { css, keyframes } from 'styled-components';
import BareButton from '../../styles/shared/button';
import { useDispatch } from 'react-redux';
import Shape from '../../../model/shapes';
import SourcePanel from './source-panel';
import useClickOutside from '../../../lib/hooks/click-outside';
import { blurInputOnEnter } from '../utils';
import { ShapeActionCreators } from '../../../store/shapes/actions';

type Props = {
  shape: Shape;
  isActive: boolean;
};

const ShapeCard = (props: Props) => {
  const { shape, isActive } = props;
  const [isSourcePanelVisible, setIsSourceVisible] = useState(false);
  const dispatch = useDispatch();
  const wrapperRef = useRef(null);
  const inputRef = useRef<HTMLInputElement>(null);

  useClickOutside(
    wrapperRef,
    useCallback(() => setIsSourceVisible(false), []),
  );

  const deleteShape = useCallback(() => dispatch(ShapeActionCreators.deleteShape(shape)), [dispatch, shape]);

  const setShapeActive = useCallback(() => dispatch(ShapeActionCreators.setCurrentShapeId(shape._id)), [
    dispatch,
    shape._id,
  ]);

  const setShapeId = useCallback((newId: string) => dispatch(ShapeActionCreators.setShapeId(shape, newId)), [
    dispatch,
    shape,
  ]);

  const toggleIsSourcePanelVisible = useCallback(() => setIsSourceVisible(!isSourcePanelVisible), [
    isSourcePanelVisible,
  ]);

  const handleShapeIdInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setShapeId(event.target.value);
  };

  const handleCloseButton = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.stopPropagation();
    deleteShape();
  };

  useEffect(() => {
    if (!isActive || !inputRef.current) {
      return;
    }
    inputRef.current.focus();
  }, [isActive]);

  return (
    <>
      <Wrapper isActive={isActive} onClick={setShapeActive} ref={wrapperRef}>
        <SelectionIndicator isActive={isActive} />
        <ShapeTypeDesc>{shape.type}</ShapeTypeDesc>
        <IdInput value={shape.id} onChange={handleShapeIdInput} onKeyPress={blurInputOnEnter} ref={inputRef} />
        <CloseButton className="fa fa-close" onClick={handleCloseButton} />
        <SourceButton onClick={toggleIsSourcePanelVisible}>{'{}'}</SourceButton>
        <AttribButton className="fa fa-caret-down" />
        <SourcePanel isVisible={isSourcePanelVisible}>{shape.toString(shape)}</SourcePanel>
      </Wrapper>
    </>
  );
};

export default ShapeCard;

const popUp = keyframes`
  from {
   opacity: 0;
  }
  to {
    opacity: auto;
  }
`;

const Wrapper = styled.div<{ isActive: boolean }>`
  ${props => (props.theme.isDarkMode ? wrapperDarkModeStyle : wrapperLightModeStyle)};
  opacity: ${props => (props.isActive ? 1 : 0.8)};
  position: relative;
  display: grid;
  align-items: center;
  align-items: end;
  grid-template: 1fr 2fr 1fr / 5% 90% 5%;
  width: 100%;
  box-sizing: border-box;
  border-width: 1px 0 0 0;
  border-radius: 3px;
  border-style: solid;
  animation: ${popUp} 100ms;

  &:hover {
    opacity: 1;
    cursor: pointer;
  }
`;

const SelectionIndicator = styled.div<{ isActive: boolean }>`
  display: ${props => (props.isActive ? 'auto' : 'none')};
  grid-area: 1 / 1 / -1 / 1;
  width: 25%;
  height: 100%;
  background-color: #ffff006b;
  border-radius: 0 3px 3px 0;
`;

const wrapperLightModeStyle = css`
  background-color: ${props => props.theme.controlsBackgroundColor};
  border-color: white;
`;
const wrapperDarkModeStyle = css`
  background-color: #6b6b6b;
  border-color: #323232;
`;

const IdInput = styled.input`
  grid-area: 2 / 2;
  background-color: transparent;
  color: ${props => (props.theme.isDarkMode ? 'white' : '#515151')};
  outline: none;
  border: none;
  font-size: 1.2em;
  font-weight: 200;
  &:focus {
    background-color: #80808033;
  }
`;

const cardHeaderStyle = css`
  margin: 0;
  color: ${props => (props.theme.isDarkMode ? 'white' : '#515151')};
  font-size: 0.6em;
`;

const ShapeTypeDesc = styled.code`
  ${cardHeaderStyle}
  grid-area: 1 / 2;
`;

const CardButton = styled(BareButton)`
  ${cardHeaderStyle}
  justify-self: end;
  align-self: center;
  padding: 3px;
  border-radius: 3px;
  &:hover {
    background-color: #80808033;
  }

  &:active {
    background-color: #fffdfd70;
  }
`;

const CloseButton = styled(CardButton)`
  grid-area: 1 / 3;
`;

const SourceButton = styled(CardButton)`
  grid-area: 3/3;
  padding: 0px 3px;
  font-weight: 700;
`;

const AttribButton = styled(BareButton)`
  ${cardHeaderStyle}
  padding: 0;
  grid-area: 3 / 2;
  border-radius: 3px;
  &:hover {
    background-color: #80808033;
  }

  &:active {
    background-color: #fffdfd70;
  }
`;

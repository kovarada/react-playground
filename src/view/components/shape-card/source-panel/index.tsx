import React from 'react';
import styled, { css } from 'styled-components';
import BareButton from '../../../styles/shared/button';
import { copyInputToClipboard } from '../../utils';

type Props = {
  children: string;
  isVisible: boolean;
};

const SourcePanel = (props: Props) => {
  if (!props.isVisible) return null;
  const copySourceCode = () => {
    copyInputToClipboard(props.children, 'Source');
  };
  return (
    <Wrapper>
      <CopyButton onClick={copySourceCode}>Copy</CopyButton>
      <Code>{props.children.trim()}</Code>
    </Wrapper>
  );
};

export default SourcePanel;

const Wrapper = styled.div`
  ${props => (props.theme.isDarkMode ? wrapperDarkModeStyle : wrapperLightModeStyle)};
  min-width: 70%;
  min-height: 100%;
  position: absolute;
  z-index: 1;
  right: 0;
  top: 0;
  transform: translateX(101%);
  border-radius: 3px;
  box-shadow: inset 0px 0px 3px 0px rgba(0, 0, 0, 0.25);
  display: grid;
  grid-template: 15px 1fr 15px / 15px 1fr 15px;
  cursor: auto;
`;

const wrapperLightModeStyle = css`
  background-color: ${props => props.theme.controlsBackgroundColor};
  border-color: white;
`;
const wrapperDarkModeStyle = css`
  background-color: #6b6b6b;
  border-color: #323232;
`;

const CopyButton = styled(BareButton)`
  position: absolute;
  margin: 5px;
  border-radius: 3px;
  padding: 3px;
  right: 0;
  background-color: #b4e8b4;
  color: white;
  font-size: 0.8em;
  &:hover {
    background-color: #c2e6c2;
  }
  &:active {
    background-color: #d9ffd9;
  }
`;

const Code = styled.pre`
  color: ${props => (props.theme.isDarkMode ? 'white' : 'auto')};
  grid-area: 2/2;
  margin: 0;
  max-width: 20vw;
  white-space: break-spaces;
`;

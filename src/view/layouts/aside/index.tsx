import React from 'react';
import styled from 'styled-components';
import { borderStyle } from '../../styles/shared/borders';
import { getShapes, getCurrentShapeId } from '../../../store/shapes/selectors';
import ShapeCard from '../../components/shape-card';
import { useSelector } from 'react-redux';

const Aside = () => {
  const shapes = useSelector(getShapes);
  const currentShapeId = useSelector(getCurrentShapeId);
  return (
    <Wrapper>
      {shapes.map(shape => (
        <ShapeCard shape={shape} key={shape._id} isActive={currentShapeId === shape._id} />
      ))}
    </Wrapper>
  );
};

export default Aside;

const Wrapper = styled.aside`
  ${borderStyle}
  border-width: 0 1px 0 0;
  grid-area: 2 / 1;
`;

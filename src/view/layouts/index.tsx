import React from 'react';
import styled from 'styled-components';
import Header from './header';

const withLayout = (ChildComponent: React.FC<any>) => {
  const componentWithLayout = (props: any) => {
    return (
      <Main>
        <Header />
        <ChildComponent {...props} />
      </Main>
    );
  };
  return componentWithLayout;
};

export default withLayout;

const Main = styled.main`
  display: grid;
  grid-template: 6vh 1fr / 15em 1fr 15em;
  height: 100vh;
  width: 100vw;
  overflow-y: auto;
`;

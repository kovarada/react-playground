import React from 'react';
import styled from 'styled-components';
import ThemeButton from './theme-button';
import { headerButtonStyle } from './style';
import PageButton from './page-button';

const Header = () => {
  return (
    <Wrapper>
      <ButtonsWrapper>
        <PageButton />
        <ThemeButton />
        <GitLabLogo
          className="fa fa-gitlab"
          href="https://gitlab.fel.cvut.cz/czm/frontend/react-guide"
          rel="noopener noreferrer"
          target="_blank"
        />
      </ButtonsWrapper>
    </Wrapper>
  );
};

export default Header;

const Wrapper = styled.header`
  grid-column: 1 / -1;
  background-color: #282c34;
  display: grid;
  grid-template: 1fr / 1fr 5fr 1fr;
  font-size: calc(10px + 2vmin);
  color: white;
  position: sticky;
  top: 0;
  z-index: 2;
`;

const GitLabLogo = styled.a`
  ${headerButtonStyle}
  grid-column: 1;
  line-height: 1.8em;
  text-align: center;
  text-decoration: none;
  color: white;
`;

const ButtonsWrapper = styled.div`
  grid-column: 3;
  display: flex;
  justify-content: space-around;
`;

import styled, { css } from 'styled-components';
import BareButton from '../../styles/shared/button';

export const headerButtonStyle = css`
  &:hover {
    color: #ffffff87;
    cursor: pointer;
  }

  &:active {
    color: gray;
  }
`;

export const HeaderButton = styled(BareButton)`
  ${headerButtonStyle}
`;

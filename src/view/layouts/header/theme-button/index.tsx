import React from 'react';
import styled from 'styled-components';
import { useDarkmode } from '../../../../lib/hooks/theme';
import { HeaderButton } from '../style';

const ThemeButton = () => {
  const [isDarkMode, toggleDarkMode] = useDarkmode();
  return <Button className={`fa fa-${isDarkMode ? 'sun-o' : 'moon-o'}`} onClick={toggleDarkMode} />;
};

export default ThemeButton;

const Button = styled(HeaderButton)`
  color: ${props => (props.theme.isDarkMode ? '#ffff99' : 'white')};
  line-height: 1.8em;
`;

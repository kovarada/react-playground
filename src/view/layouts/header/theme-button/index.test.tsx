import { render, fireEvent } from '@testing-library/react';
import ThemeButton from '.';
import React from 'react';
import { Theme } from '../../../styles/types';
import { defaultTheme } from '../../../styles/theme';
import { useTheme } from '../../../../lib/hooks/theme';
import { ThemeProvider } from 'styled-components';
import { screen } from '@testing-library/dom';

function setup(inputTheme: Theme) {
  const ret: { theme: any; getByRole: any } = { theme: null, getByRole: null };

  function ThemeContainer(props: { children: React.ReactNode; theme: Theme }) {
    const theme = useTheme(props.theme);
    ret.theme = theme;
    return <ThemeProvider theme={theme}>{props.children}</ThemeProvider>;
  }

  const { getByRole } = render(
    <ThemeContainer theme={inputTheme}>
      <ThemeButton />
    </ThemeContainer>,
  );
  ret.getByRole = getByRole;
  return ret;
}
it('should toggle theme on click event', () => {
  const { theme, getByRole } = setup(defaultTheme);
  screen.debug(getByRole('button'));
  fireEvent.click(getByRole('button'));
});

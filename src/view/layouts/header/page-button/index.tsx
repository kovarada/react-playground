import React from 'react';
import styled from 'styled-components';
import { HeaderButton } from '../style';
import { Link, useLocation } from 'react-router-dom';

const PageButton = () => {
  const location = useLocation();
  const isHomePage = location.pathname === '/';
  return (
    <RouterLink to={isHomePage ? '/about' : '/'}>
      <Button>{isHomePage ? 'About' : 'Home'}</Button>
    </RouterLink>
  );
};

export default PageButton;

const Button = styled(HeaderButton)``;

const RouterLink = styled(Link)`
  color: white;
  display: flex;
  justify-content: center;
  align-items: center;
`;

import React from 'react';
import withLayout from '../../layouts';
import styled from 'styled-components';

type Props = {
  message: string;
};

const ErrorPage = (props: Props) => {
  return (
    <Wrapper>
      <Title>{props.message}</Title>
    </Wrapper>
  );
};

export default withLayout(ErrorPage);

const Wrapper = styled.section`
  margin: auto;
  grid-area: 2 / 2;
  display: flex;
  align-items: center;
`;

const Title = styled.h1`
  font-size: 5em;
  font-weight: 300;
  color: pink;
`;

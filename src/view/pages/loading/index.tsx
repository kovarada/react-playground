import React from 'react';
import styled, { keyframes, css } from 'styled-components';
import withLayout from '../../layouts';

const Loading = () => {
  return <Wrapper />;
};

export default withLayout(Loading);

const slide = keyframes`
  from{
    background-position-x: 0%;
  }
  to {
    background-position-x: 100%;
  }
`;

const darkBackgroundGradient = css`
  background: linear-gradient(80deg, #404040, #404040, #80808047, #404040, #404040);
`;

const lightBackgroundGradient = css`
  background: linear-gradient(80deg, white, white, #808080a1, white, white);
`;

const Wrapper = styled.section`
  ${props => (props.theme.isDarkMode ? darkBackgroundGradient : lightBackgroundGradient)};
  grid-area: 2 / 1 / 4 / 4;
  width: 100%;
  height: 100%;
  background-size: 400% 100%;
  background-position-x: 100px;
  animation: ${slide} 1s infinite;
`;

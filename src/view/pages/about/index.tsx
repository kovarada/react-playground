import React from 'react';
import styled from 'styled-components';
import withLayout from '../../layouts';

const About = () => {
  return (
    <Wrapper>
      <TitleWrapper>
        <Title id="hub">hub</Title>
        <Title id="fel">fel</Title>
      </TitleWrapper>
      <Title>training project</Title>
    </Wrapper>
  );
};

export default withLayout(About);

const Wrapper = styled.section`
  margin: auto;
  grid-area: 2 / 2;
  display: flex;
  align-items: center;

  & > div {
    margin-right: 1em;
  }

  & > h1 {
    color: grey;
  }
`;

const TitleWrapper = styled.div`
  display: flex;
  cursor: default;

  & > #fel:before {
    content: '.';
  }

  &:hover {
    flex-direction: row-reverse;
    & > h1 {
      padding: 0.1em;
    }
    #hub {
      background-color: pink;
      color: white;
      border-radius: 13px;
    }
    #fel:before {
      content: '';
    }
  }
`;

const Title = styled.h1`
  color: ${props => (props.theme.isDarkMode ? 'white' : 'black')};
  font-size: 3em;
  font-weight: 300;
`;

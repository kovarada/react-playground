import React from 'react';
import withLayout from '../../layouts';
import Aside from '../../layouts/aside';
import DrawPanel from '../../components/draw-panel';
import { ExportPanel } from '../../components/export-panel';
import { Snackbar } from './Snackbar';

const Home = () => {
  return (
    <>
      <Aside />
      <DrawPanel />
      <ExportPanel />
      <Snackbar />
    </>
  );
};

export default withLayout(Home);

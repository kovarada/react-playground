/* eslint-disable react/prop-types */
import React, { useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import styled, { keyframes } from 'styled-components';
import { useDispatchAction } from '../../../lib/hooks/dispatch-action';
import { ActionType } from '../../../store/ActionType';
import { State } from '../../../store/types';

const ttl = 2000;

export const Snackbar: React.FC = () => {
  const timeout = useRef<number | null>(null);
  const deleteMessage = useDispatchAction({ type: ActionType.DELETE_MESSAGE });
  const message = useSelector<State, string | null>(({ messageState }) => messageState.message);

  useEffect(() => {
    if (!message) {
      return;
    }

    if (timeout.current !== null) {
      clearTimeout(timeout.current);
    }

    timeout.current = setTimeout(deleteMessage, ttl);
  }, [message, deleteMessage]);
  return <S.Wrapper onClick={deleteMessage}>{message}</S.Wrapper>;
};

const S = {
  Wrapper: styled.div`
    position: absolute;
    margin: 15px;
    bottom: 0;
    right: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 1rem;
    width: 20rem;
    box-shadow: 0 0 14px 2px #bdbcbce8;
    cursor: pointer;
    animation: ${keyframes`70% {opacity:1} 100% {opacity: 0}`} ${ttl}ms linear;

    &:empty {
      display: none;
    }
  `,
};

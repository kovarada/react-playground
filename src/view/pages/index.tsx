import React, { Suspense, lazy } from 'react';
import GlobalStyle from '../styles/global';
import { ThemeProvider } from 'styled-components';
import { Route, Switch, Redirect } from 'react-router-dom';
import { defaultTheme } from '../../view/styles/theme';
import { useTheme } from '../../lib/hooks/theme';
import useDocumentTitle from '../../lib/hooks/document-title';
import { useSelector } from 'react-redux';
import { getCurrentShapeId } from '../../store/shapes/selectors';
import Home from './home';
import Loading from './loading';
import ErrorPage from './error';
import ErrorBoundary from '../components/error-boundary';

const About = lazy(() => import('./about'));

function App() {
  const theme = useTheme(defaultTheme);
  const currentShapeId = useSelector(getCurrentShapeId);
  useDocumentTitle(`${currentShapeId ? '*' : ''}Map Builder`);

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <ErrorBoundary fallback={<ErrorPage message="Connection failure" />}>
        <Suspense fallback={<Loading />}>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/about" component={About} />
            <Route path="/unknown">
              <ErrorPage message="Page not found" />
            </Route>
            {/* <Redirect to="/unknown" /> */}
          </Switch>
        </Suspense>
      </ErrorBoundary>
    </ThemeProvider>
  );
}

export default App;

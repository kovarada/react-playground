import React from 'react';
import { render } from '@testing-library/react';
import App from '.';
import { Provider } from 'react-redux';
import store from '../store';

test('renders learn react link', () => {
  const { getByText } = render(
    <Provider store={store}>
      <App />
    </Provider>,
  );
});

enum ShapeType {
  POLYGON = 'polygon',
  LABEL = 'label',
  NODE = 'node',
}

export default ShapeType;

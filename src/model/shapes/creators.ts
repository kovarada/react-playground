import { Polygon, Label, Node } from '.';
import { SVG_ELEMENT_DIMS, SVG_VIEWBOX_DIMS } from '../../utils/constants';

class ShapeCreators {
  static polygon(points = '') {
    const polygon = new Polygon(points);
    polygon.id = `Polygon${polygon._id}`;
    return polygon;
  }

  static label(point: number[]) {
    const x = (point[0] * SVG_ELEMENT_DIMS.width) / SVG_VIEWBOX_DIMS.width;
    const y = (point[1] * SVG_ELEMENT_DIMS.height) / SVG_VIEWBOX_DIMS.height;
    const label = { x: x, y: y, text: 'text' };
    const labelInstance = new Label(label);
    labelInstance.id = `Label${labelInstance._id}`;
    return labelInstance;
  }

  static node(point: number[]) {
    const node = { x: point[0], y: point[1], children: [] };
    const nodeInstance = new Node(node);
    nodeInstance.id = `Node${nodeInstance._id}`;
    return nodeInstance;
  }
}

export default ShapeCreators;

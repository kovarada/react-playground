import ShapeType from './shape-type';
import { SVG_FLOORPLAN_MAP_DIMS, SVG_ELEMENT_DIMS } from '../../utils/constants';

export type ShapeAttribsUnion = PolygonAttribs | LabelAttribs | NodeAttribs;

const idGenerator = (function* () {
  for (let id = 1; true; id++) {
    yield String(id);
  }
})();

abstract class Shape<T = ShapeAttribsUnion> {
  public readonly _id: string;
  constructor(public type: ShapeType, public id = '', public historyIdx: number = 0, public history: T[] = []) {
    this._id = idGenerator.next().value;
    this.type = type;
  }

  abstract toString: (shape: Shape<any>, floor?: number) => string;
}

export default Shape;

export type PolygonAttribs = string;

export class Polygon extends Shape<PolygonAttribs> {
  constructor(points: PolygonAttribs) {
    super(ShapeType.POLYGON);
    this.history = [points];
  }

  toString = (polygon: Polygon) =>
    `{
  id: "${polygon.id}",
  points: "${polygon.history[polygon.historyIdx]}",
},`;
}

export type LabelAttribs = {
  text: string;
  x: number;
  y: number;
};

export class Label extends Shape<LabelAttribs> {
  constructor(label: LabelAttribs) {
    super(ShapeType.LABEL);
    this.history = [label];
  }

  toString = (label: Label) =>
    `{
  id: "${label.id}",
  x: ${(label.history[label.historyIdx].x * SVG_FLOORPLAN_MAP_DIMS.width) / SVG_ELEMENT_DIMS.width},
  y: ${(label.history[label.historyIdx].y * SVG_FLOORPLAN_MAP_DIMS.height) / SVG_ELEMENT_DIMS.height},
},`;
}

export type NodeAttribs = {
  children: Node[];
  x: number;
  y: number;
};

export class Node extends Shape<NodeAttribs> {
  constructor(node: NodeAttribs) {
    super(ShapeType.NODE);
    this.history = [node];
  }

  toString = (node: Node, floor = 0) => {
    const nodeAttribs = node.history[node.historyIdx];
    return ` ["${node.id}", [${floor}, ${nodeAttribs.x}, ${nodeAttribs.y}], [${nodeAttribs.children.map(
      childNode => `"${childNode.id}"`,
    )}]],`;
  };
}

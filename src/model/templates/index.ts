import { Node, Polygon } from '../shapes';
import ShapeCreators from '../shapes/creators';

export type RoomEntry = { id: string; points: string };
export type NodeEntry = [string, [number, number, number], string[]];

export function createRoomPolygons(rooms: RoomEntry[]): Polygon[] {
  return rooms.map(room => {
    const newPolygon = ShapeCreators.polygon(room.points);
    newPolygon.id = room.id;
    return newPolygon;
  });
}

export function createNodes(nodeEntries: NodeEntry[]): Node[] {
  const newNodes = nodeEntries.map(([id, [, left, top], childrenIds]) => {
    const newNode = ShapeCreators.node([left, top]);
    newNode.id = id;
    return [newNode, childrenIds] as [typeof newNode, string[]];
  });

  newNodes.forEach(([node, childrenIds]) => {
    node.history[node.historyIdx].children = childrenIds
      .map(id => newNodes.find(([node]) => node.id === id)?.[0])
      .filter(node => !!node) as Node[];
  });
  return newNodes.map(([node]) => node);
}

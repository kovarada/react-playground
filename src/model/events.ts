export interface SVGMouseEvent extends React.MouseEvent<SVGSVGElement, MouseEvent> {
  target: SVGMouseEventTarget;
}

interface SVGMouseEventTarget extends EventTarget {
  nodeName: string;
}

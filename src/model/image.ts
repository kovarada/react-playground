interface Image {
  href: string;
  width: string;
  height: string;
}

export default Image;

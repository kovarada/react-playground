export const SVG_ELEMENT_DIMS = { width: 650, height: 487.5 };
export const SVG_VIEWBOX_DIMS = { width: 580, height: 400 };
export const SVG_FLOORPLAN_MAP_DIMS = { width: 720, height: 540 };

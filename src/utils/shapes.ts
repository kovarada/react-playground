import Shape from '../model/shapes';

export function getShapeAttribs(shape: Shape | null) {
  return shape?.history[shape.historyIdx];
}

export function isShapeUpToDate(shape: Shape | null) {
  if (!shape) return false;
  return shape.historyIdx === shape.history.length - 1;
}

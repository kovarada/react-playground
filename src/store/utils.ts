import { ShapesState } from './shapes/types';
import Shape, { ShapeAttribsUnion } from '../model/shapes';
import { State } from './types';

export function findShapeById(shapesState: ShapesState, _id: string | null) {
  if (!_id) return null;
  return shapesState.shapes.find(shape => shape._id === _id);
}

export function updateShapeArray(shapes: Shape[], newShape: Shape) {
  return shapes.map(shape => (shape._id === newShape._id ? { ...newShape } : { ...shape }));
}

export function updateShapeHistory(prevShape: Shape, newAttribs: ShapeAttribsUnion) {
  const newHistory = [...prevShape.history.slice(0, prevShape.historyIdx + 1).concat(newAttribs)];
  return { ...prevShape, history: newHistory, historyIdx: newHistory.length - 1 };
}

export function getUndoableSlice(state: State) {
  return state.undoable;
}

import { ActionType } from '../ActionType';

export class LabelActionCreators {
  static setLabelPosition(position: number[]) {
    return { type: ActionType.SET_LABEL_POSITION, position: position };
  }

  static addLabel(point: number[]) {
    return { type: ActionType.ADD_LABEL, point: point };
  }
}

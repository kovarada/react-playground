import { Action } from '../types';

export interface AddLabelAction extends Action {
  point: number[];
}

export interface SetLabelPositionAction extends Action {
  position: number[];
}

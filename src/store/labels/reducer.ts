import { INIT_SHAPES_STATE } from '../shapes/reducer';
import { ActionType } from '../ActionType';
import ShapeCreators from '../../model/shapes/creators';
import { ShapesState } from '../shapes/types';
import { AddLabelAction, SetLabelPositionAction } from './types';
import { findShapeById, updateShapeArray, updateShapeHistory } from '../utils';
import { LabelAttribs, Label } from '../../model/shapes';
import { getShapeAttribs } from '../../utils/shapes';

export type LabelActions = AddLabelAction & SetLabelPositionAction;

export function labelsReducer(prevState = INIT_SHAPES_STATE, action: LabelActions) {
  switch (action.type) {
    case ActionType.ADD_LABEL:
      return applyAddLabel(prevState, action);
    case ActionType.SET_LABEL_POSITION:
      return applySetLabelPosition(prevState, action);
    default:
      return prevState;
  }
}

function applyAddLabel(prevState: ShapesState, action: AddLabelAction) {
  const newLabel = ShapeCreators.label(action.point);
  const newShapes = prevState.shapes.concat(newLabel);
  return { ...prevState, shapes: newShapes, currentId: newLabel.id };
}

function applySetLabelPosition(prevState: ShapesState, action: SetLabelPositionAction) {
  const currentLabel = findShapeById(prevState, prevState.currentId) as Label;
  const [x, y] = action.position;
  const newAttribs = { ...(getShapeAttribs(currentLabel) as LabelAttribs), x, y };
  const newLabel = updateShapeHistory(currentLabel, newAttribs);
  const newShapes = updateShapeArray(prevState.shapes, newLabel);
  return { ...prevState, shapes: newShapes };
}

import { State } from '../types';
import { findShapeById, getUndoableSlice } from '../utils';
import ShapeType from '../../model/shapes/shape-type';

export function getCurrentShape(state: State) {
  return (
    findShapeById(getUndoableSlice(state).present.shapesState, getUndoableSlice(state).present.shapesState.currentId) ||
    null
  );
}

export function getCurrentShapeId(state: State) {
  return getUndoableSlice(state).present.shapesState.currentId;
}

export function getShapes(state: State) {
  return getUndoableSlice(state).present.shapesState.shapes;
}

export function getCurrentShapeType(state: State) {
  return getUndoableSlice(state).present.shapesState.currentShapeType || ShapeType.POLYGON;
}

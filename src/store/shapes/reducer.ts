import { ActionType } from '../ActionType';
import { ShapesState, SetCurrentShapeAction, DeleteShapeAction, SetShapeIdAction, SetShapeTypeAction } from './types';
import polygonsReducer, { PolygonAction } from '../polygons/reducer';
import { updateShapeArray, findShapeById, updateShapeHistory } from '../utils';
import ShapeType from '../../model/shapes/shape-type';
import { labelsReducer, LabelActions } from '../labels/reducer';
import nodesReducer, { NodeActions } from '../nodes/reducer';
import { getShapeAttribs } from '../../utils/shapes';
import Shape, { NodeAttribs, Node } from '../../model/shapes';
import { TemplateAction, templateReducer } from '../templates';

export const INIT_SHAPES_STATE: ShapesState = {
  shapes: [],
  currentId: null,
  currentShapeType: ShapeType.POLYGON,
};

type ShapeAction = SetCurrentShapeAction &
  PolygonAction &
  DeleteShapeAction &
  SetShapeIdAction &
  SetShapeTypeAction &
  LabelActions &
  TemplateAction &
  NodeActions;

function shapesReducer(prevState = INIT_SHAPES_STATE, action: ShapeAction) {
  switch (action.type) {
    case ActionType.SAVE_SHAPE:
      return applySaveShape(prevState);
    case ActionType.SET_CURRENT_SHAPE_ID:
      return applySetCurrentShapeId(prevState, action);
    case ActionType.DELETE_SHAPE:
      return applyDeleteShape(prevState, action);
    case ActionType.SET_SHAPE_ID:
      return setShapeId(prevState, action);
    case ActionType.SET_CURRENT_SHAPE_TYPE:
      return applySetCurrentShapeType(prevState, action);
    case ActionType.UNDO_SHAPE:
      return applyHistory(prevState, action.type);
    case ActionType.REDO_SHAPE:
      return applyHistory(prevState, action.type);
    default:
      return prevState;
  }
}

function rootShapeReducer(prevState = INIT_SHAPES_STATE, action: ShapeAction) {
  prevState = polygonsReducer(prevState, action);
  prevState = labelsReducer(prevState, action);
  prevState = nodesReducer(prevState, action);
  prevState = templateReducer(prevState, action);
  return shapesReducer(prevState, action);
}

export default rootShapeReducer;

function applySaveShape(prevState: ShapesState) {
  const { shapes, currentId } = prevState;
  if (!currentId) return prevState;
  return { ...prevState, currentId: null, shapes: [...shapes] };
}

function applySetCurrentShapeId(prevState: ShapesState, action: SetCurrentShapeAction) {
  const { shapes } = prevState;
  const newShape = shapes.find(shape => shape._id === action._id);
  const newCurrentId = newShape ? action._id : null;
  const newShapeType = newShape ? newShape.type : prevState.currentShapeType;
  return { ...prevState, currentId: newCurrentId, currentShapeType: newShapeType };
}

function applyDeleteShape(prevState: ShapesState, action: DeleteShapeAction) {
  const { shapes } = prevState;
  let newShapes = shapes.filter(shape => shape._id !== action._id);
  if (prevState.currentShapeType === ShapeType.NODE) {
    newShapes = removeNodeFromParents(newShapes, action._id);
  }
  return { ...prevState, currentId: null, shapes: [...newShapes] };
}

function setShapeId(prevState: ShapesState, action: SetShapeIdAction) {
  const { shapes } = prevState;
  const newShape = action.shape;
  newShape.id = action.newId;
  let newShapes = updateShapeArray(shapes, newShape);
  if (prevState.currentShapeType === ShapeType.NODE) {
    newShapes = updateNodesChldren(newShapes, action.shape as Node);
  }
  return { ...prevState, shapes: [...newShapes] };
}

function applySetCurrentShapeType(prevState: ShapesState, action: SetShapeTypeAction) {
  return { ...prevState, currentId: null, currentShapeType: action.shapeType };
}

export function applyHistory(prevState: ShapesState, type: ActionType) {
  const { shapes: currentShapes, currentId } = prevState;
  const currentShape = findShapeById(prevState, currentId);
  if (!currentShape) return prevState;
  const currentHistoryIdx = currentShape.historyIdx;
  const check =
    type === ActionType.REDO_SHAPE ? +(currentHistoryIdx < currentShape.history.length - 1) : -(currentHistoryIdx > 0);
  const newHistoryIdx = currentHistoryIdx + 1 * check;
  const newShape = { ...currentShape, historyIdx: newHistoryIdx };
  const newShapes = updateShapeArray(currentShapes, newShape);
  return { ...prevState, shapes: [...newShapes] };
}

function removeNodeFromParents(shapes: Shape[], _id: string) {
  return shapes.map(shape => {
    if (shape.type === ShapeType.NODE) {
      const nodeAttribs = getShapeAttribs(shape) as NodeAttribs;
      const newChildren = nodeAttribs.children.filter(child => child._id !== _id);
      return updateShapeHistory(shape, { ...nodeAttribs, children: newChildren });
    }
    return shape;
  });
}

function updateNodesChldren(shapes: Shape[], updatedNode: Node) {
  return shapes.map(shape => {
    if (shape.type === ShapeType.NODE) {
      const nodeAttribs = getShapeAttribs(shape) as NodeAttribs;
      const newChildren = nodeAttribs.children.map(child => (child._id !== updatedNode._id ? child : updatedNode));
      return updateShapeHistory(shape, { ...nodeAttribs, children: newChildren });
    }
    return shape;
  });
}

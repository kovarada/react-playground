import { ActionType } from '../ActionType';
import Shape from '../../model/shapes';
import ShapeType from '../../model/shapes/shape-type';
import { PolygonActionCreators } from '../polygons/actions';
import { LabelActionCreators } from '../labels/actions';
import { NodeActionCreators } from '../nodes/actions';

export class ShapeActionCreators {
  static deleteShape(shape: Shape) {
    return { type: ActionType.DELETE_SHAPE, _id: shape._id };
  }

  static setCurrentShapeId(_id: string | null) {
    return { type: ActionType.SET_CURRENT_SHAPE_ID, _id: _id };
  }

  static setCurrentShapeType(shapeType: ShapeType) {
    return { type: ActionType.SET_CURRENT_SHAPE_TYPE, shapeType: shapeType };
  }

  static draw(shapeType: ShapeType, point: number[]) {
    const [x, y] = point;
    switch (shapeType) {
      case ShapeType.LABEL:
        return LabelActionCreators.addLabel(point);
      case ShapeType.NODE:
        return NodeActionCreators.addNode(point);
      default:
        const newPoint = `${x.toFixed()},${y.toFixed()} `;
        return PolygonActionCreators.addPoint(newPoint);
    }
  }

  static setShapeId(shape: Shape, newId: string) {
    return { type: ActionType.SET_SHAPE_ID, shape: shape, newId: newId };
  }

  static undoShape() {
    return { type: ActionType.UNDO_SHAPE };
  }

  static redoShape() {
    return { type: ActionType.REDO_SHAPE };
  }

  static saveShape() {
    return { type: ActionType.SAVE_SHAPE };
  }
}

import { Action } from '../types';
import Shape from '../../model/shapes';
import ShapeType from '../../model/shapes/shape-type';

export interface ShapesState {
  shapes: Shape[];
  currentId: string | null;
  currentShapeType: ShapeType;
}

export interface SetCurrentShapeAction extends Action {
  _id: string;
}

export interface DeleteShapeAction extends Action {
  _id: string;
}

export interface SetShapeIdAction extends Action {
  shape: Shape;
  newId: string;
}

export interface SetShapeTypeAction extends Action {
  shapeType: ShapeType;
}

import { combineReducers } from 'redux';
import imageReducer from './image/reducer';
import undoable from 'redux-undo';
import shapesReducer from './shapes/reducer';
import { messageReducer } from './message/reducer';

const rootReducer = undoable(combineReducers({ shapesState: shapesReducer, imageState: imageReducer }));
export default combineReducers({ undoable: rootReducer, messageState: messageReducer });

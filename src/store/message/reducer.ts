import { ActionType } from '../ActionType';
import { Action } from '../types';

export type MessageState = { message: string | null };

const INIT_STATE: MessageState = { message: null };

type MessageAction = { type: ActionType; message: string };

export function messageReducer(prevState = INIT_STATE, action: Action & MessageAction) {
  switch (action.type) {
    case ActionType.SET_MESSAGE:
      return { ...prevState, message: action.message };
    case ActionType.DELETE_MESSAGE:
      return { ...prevState, message: null };
    default:
      return prevState;
  }
}

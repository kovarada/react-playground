import { ActionType } from '../ActionType';
import { AddPointAction, SetPointsAction, RemovePointAction } from './types';
import ShapeCreators from '../../model/shapes/creators';
import { ShapesState } from '../shapes/types';
import { findShapeById, updateShapeArray, updateShapeHistory } from '../utils';
import Shape, { Polygon } from '../../model/shapes';
import { INIT_SHAPES_STATE } from '../shapes/reducer';
import { getShapeAttribs } from '../../utils/shapes';

export type PolygonAction = AddPointAction & SetPointsAction & RemovePointAction;

function polygonsReducer(prevState = INIT_SHAPES_STATE, action: PolygonAction) {
  switch (action.type) {
    case ActionType.ADD_POINT:
      return applyAddPoint(prevState, action);
    case ActionType.SET_POINTS:
      return applySetPoints(prevState, action);
    default:
      return prevState;
  }
}

export default polygonsReducer;

export function applyAddPoint(prevState: ShapesState, action: AddPointAction) {
  const { shapes: currentShapes, currentId } = prevState;
  const currentShape = findShapeById(prevState, currentId) as Polygon;
  if (!currentShape) {
    const newShape = ShapeCreators.polygon(action.point);
    const newShapes = currentShapes.concat(newShape);
    return { ...prevState, currentId: newShape._id, shapes: newShapes };
  } else {
    const newPoints = getShapeAttribs(currentShape) + action.point;
    const newShape = updateShapeHistory(currentShape, newPoints);
    const newShapes = updateShapeArray(currentShapes, newShape);
    return { ...prevState, shapes: [...newShapes] };
  }
}

export function applySetPoints(prevState: ShapesState, action: SetPointsAction) {
  const { shapes: currentShapes, currentId } = prevState;
  const currentShape = findShapeById(prevState, currentId);
  let newShape: Shape;
  if (!currentShape) {
    newShape = ShapeCreators.polygon(action.points);
    currentShapes.push(newShape);
    return { ...prevState, currentId: newShape._id, shapes: [...currentShapes] };
  } else {
    newShape = updateShapeHistory(currentShape!, action.points);
    const newShapes = updateShapeArray(currentShapes, newShape);
    return { ...prevState, shapes: newShapes };
  }
}

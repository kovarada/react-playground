import { Action } from '../types';

export interface AddPointAction extends Action {
  point: string;
}

export interface SetPointsAction extends Action {
  points: string;
}

export interface RemovePointAction extends Action {}

import { ActionType } from '../ActionType';

export class PolygonActionCreators {
  static addPoint(point: string) {
    return { type: ActionType.ADD_POINT, point: point };
  }

  static setPoints(points: string) {
    return { type: ActionType.SET_POINTS, points: points };
  }
}

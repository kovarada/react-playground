import { State } from '../types';
import { getUndoableSlice } from '../utils';

export function canUndo(state: State) {
  return getUndoableSlice(state).past.length > 0;
}

export function canRedo(state: State) {
  return getUndoableSlice(state).future.length > 0;
}

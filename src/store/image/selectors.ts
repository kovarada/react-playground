import { State } from '../types';
import { getUndoableSlice } from '../utils';

export function getImage(state: State) {
  return {
    ...getUndoableSlice(state).present.imageState.image,
    isVisible: getUndoableSlice(state).present.imageState.isVisible,
  };
}

export function isImageVisible(state: State) {
  return getUndoableSlice(state).present.imageState.isVisible;
}

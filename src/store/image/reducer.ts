import { data } from '../../model/templates/data';
import { ActionType } from '../ActionType';
import { TemplateAction } from '../templates';
import { ImageState, SetImageAction } from './types';

const defaultImage = { href: '', width: '100%', height: '100%' };

const INIT_STATE: ImageState = { image: defaultImage, isVisible: true };

type ImageAction = SetImageAction & TemplateAction;

function imageReducer(prevState = INIT_STATE, action: ImageAction) {
  switch (action.type) {
    case ActionType.SET_IMAGE:
      return applySetImage(prevState, action);
    case ActionType.SET_TEMPLATE:
      const newImage = { ...defaultImage, href: data[action.id]?.image ?? '' };
      return applySetImage(prevState, { ...action, image: newImage });
    case ActionType.TOGGLE_IMAGE_VISIBILITY:
      return applyToggleImageVisibility(prevState);
    default:
      return prevState;
  }
}

export default imageReducer;

function applySetImage(prevState: ImageState, action: SetImageAction) {
  return { ...prevState, image: action.image, isVisible: true };
}

function applyToggleImageVisibility(prevState: ImageState) {
  return { ...prevState, isVisible: !prevState.isVisible };
}

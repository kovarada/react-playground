import { ActionType } from '../ActionType';
import Image from '../../model/image';

export class ImageActionCreators {
  static setImage(image: Image) {
    return { type: ActionType.SET_IMAGE, image: image };
  }

  static toggleImageVisibility() {
    return { type: ActionType.TOGGLE_IMAGE_VISIBILITY };
  }
}

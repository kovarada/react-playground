import { Action } from '../types';
import Image from '../../model/image';

export interface ImageState {
  image: Image;
  isVisible: boolean;
}

export interface SetImageAction extends Action {
  image: Image;
}

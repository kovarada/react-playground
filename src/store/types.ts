import { ActionType } from './ActionType';
import { ImageState } from './image/types';
import { Theme } from '../view/styles/types';
import { ShapesState } from './shapes/types';
import { MessageState } from './message/reducer';

export interface Action {
  type: ActionType;
}

export interface WithHistoryState<T> {
  present: T;
  past: T[];
  future: T[];
}

export interface State {
  undoable: WithHistoryState<{
    shapesState: ShapesState;
    imageState: ImageState;
    themeState: Theme;
  }>;
  messageState: MessageState;
}

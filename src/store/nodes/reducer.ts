import { INIT_SHAPES_STATE } from '../shapes/reducer';
import { ActionType } from '../ActionType';
import { ShapesState } from '../shapes/types';
import { AddNodeAction, ConnectNodeAction, SetNodePositionAction, SetNodesAction } from './types';
import ShapeCreators from '../../model/shapes/creators';
import { getShapeAttribs } from '../../utils/shapes';
import { Node, NodeAttribs } from '../../model/shapes';
import { updateShapeHistory, updateShapeArray, findShapeById } from '../utils';
import ShapeType from '../../model/shapes/shape-type';

export type NodeActions = AddNodeAction & ConnectNodeAction & SetNodesAction & SetNodePositionAction;

function nodesReducer(prevState = INIT_SHAPES_STATE, action: NodeActions) {
  switch (action.type) {
    case ActionType.ADD_NODE:
      return applyAddNode(prevState, action);
    case ActionType.CONNECT_NODE:
      return applyConnectNode(prevState, action);
    case ActionType.SET_NODE_POSITION:
      return applySetNodePosition(prevState, action);
    case ActionType.SET_NODES:
      return applySetNodes(prevState, action);
    default:
      return prevState;
  }
}

export default nodesReducer;

function applySetNodes(prevState: ShapesState, action: SetNodesAction) {
  const { shapes: currentShapes } = prevState;
  const newNodes = action.nodes;
  const newShapes = currentShapes.filter(shape => shape.type !== ShapeType.NODE).concat(newNodes);
  return { ...prevState, shapes: newShapes };
}

function applyAddNode(prevState: ShapesState, action: AddNodeAction) {
  const { shapes: currentShapes } = prevState;
  const newNode = ShapeCreators.node(action.point);
  const newShapes = currentShapes.concat(newNode);
  return { ...prevState, shapes: newShapes, currentId: newNode._id };
}

function applyConnectNode(prevState: ShapesState, action: ConnectNodeAction) {
  const { shapes: currentShapes, currentId } = prevState;
  const { node: nodeToConnect } = action;
  const currentShape = currentShapes.find(shape => shape._id === currentId) as Node;
  const [a, b] = connectNodes(nodeToConnect, currentShape);
  let newShapes = updateShapeArray(currentShapes, a);
  newShapes = updateShapeArray(newShapes, b);
  return { ...prevState, shapes: newShapes };
}

function connectNodes(a: Node, b: Node) {
  return [addChildToParent(b, a), addChildToParent(a, b)];
}

function addChildToParent(child: Node, parent: Node) {
  const currentNodeAttribs = getShapeAttribs(parent) as NodeAttribs;
  const currentNodeChildren = currentNodeAttribs.children;
  const nodeToConnectIdx = currentNodeChildren.findIndex(childNode => childNode._id === child._id);
  let newChildren;
  if (nodeToConnectIdx > -1) {
    currentNodeChildren.splice(nodeToConnectIdx, 1);
    newChildren = [...currentNodeChildren];
  } else {
    newChildren = currentNodeChildren.concat(child);
  }

  const newParent = updateShapeHistory(parent, { ...currentNodeAttribs, children: newChildren });
  return newParent as Node;
}

function applySetNodePosition(prevState: ShapesState, action: SetNodePositionAction) {
  const currentNode = findShapeById(prevState, prevState.currentId) as Node;
  const [x, y] = action.position;
  const newAttribs = { ...(getShapeAttribs(currentNode) as NodeAttribs), x, y };
  const newLabel = updateShapeHistory(currentNode, newAttribs);
  const newShapes = updateShapeArray(prevState.shapes, newLabel);
  return { ...prevState, shapes: newShapes };
}

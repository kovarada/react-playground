import { Action } from '../types';
import { Node } from '../../model/shapes';

export interface AddNodeAction extends Action {
  point: number[];
}

export interface ConnectNodeAction extends Action {
  node: Node;
}

export interface SetNodesAction extends Action {
  nodes: Node[];
}

export interface SetNodePositionAction extends Action {
  position: number[];
}

import { ActionType } from '../ActionType';
import { Node } from '../../model/shapes';

export class NodeActionCreators {
  static addNode(point: number[]) {
    return { type: ActionType.ADD_NODE, point };
  }
  static setNodePosition(position: number[]) {
    return { type: ActionType.SET_NODE_POSITION, position: position };
  }

  static connectNode(node: Node) {
    return { type: ActionType.CONNECT_NODE, node };
  }
}

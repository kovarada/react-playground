import { createNodes, createRoomPolygons } from '../../model/templates';
import { data } from '../../model/templates/data';
import { ActionType } from '../ActionType';
import { INIT_SHAPES_STATE } from '../shapes/reducer';
import { Action } from '../types';

export type TemplateAction = Action & { id: string };

export function templateReducer(prevState = INIT_SHAPES_STATE, action: TemplateAction) {
  switch (action.type) {
    case ActionType.SET_TEMPLATE:
      const templateData = data[action.id];
      if (!templateData) {
        return prevState;
      }
      const { rooms, nodes } = templateData;
      return { ...prevState, shapes: [...createRoomPolygons(rooms), ...createNodes(nodes)] };

    default:
      return prevState;
  }
}

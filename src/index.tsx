import React from 'react';
import ReactDOM from 'react-dom';
import App from './view/pages';
import * as serviceWorker from './serviceWorker';
import store from './store';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

const ROUTER_BASENAME = import.meta.env.VITE_ROUTER_BASENAME;

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router basename={ROUTER_BASENAME}>
        <App />
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
